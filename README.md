## Ores

* Coal => Small Loot / Negative Effects
* Iron => Large Loot (armor)
* Gold => Small Powerups
* Diamond => Large Powerups
* Copper => Small XP
* Redstone => Large XP
* Lapis =>
* Emerald => Special Modifier (swap places? blue shell?)

## Powerups

* "blue shell" equivalents

## Items

* (leather) Winged Sandals (jump boost)
* (chainmail) Running Shoes (speed)
* (iron) Long Fall Boots (no fall dmg)

* Protective Chestplate
* Fireproof Chestplate

### Tools

* (iron) Lucky Pickaxe
* (diamond) Fast Pickaxe
* (netherite) Explosive Pickaxe

* Sword of Healing
* Sword of Poison
* Electric Sword
* Thor's Hammer (single-use)

### Special

* Pocket Totems (spawn friendly mobs upon use)

## Arena Event


## Server

Use CoreProtect to reset broken ore blocks after each round

# Sounds from freesound.com:

- [gong.ogg](https://freesound.org/people/BOSS%20MUSIC/sounds/121800/)
- [ambience2.ogg](https://freesound.org/people/ecfike/sounds/128246/)
- [ambience3.ogg](https://freesound.org/people/ecfike/sounds/128247/)
- [ice.ogg](https://freesound.org/people/Anthousai/sounds/406078/)
- [shatter.ogg](https://freesound.org/people/lurpsis/sounds/444136/)
- [crystal.ogg](https://freesound.org/people/matthewHoldenSound/sounds/542560/)
- [break.ogg](https://freesound.org/people/Aurelon/sounds/422633/)
- [wall.ogg](https://freesound.org/people/NeoSpica/sounds/504618/)
- [timpani.ogg](https://freesound.org/people/Terry93D/sounds/369394/)
- [horn_soft.ogg](https://freesound.org/people/Kurlyjoe/sounds/501030/)
- [horn_bass.ogg](https://freesound.org/people/nanoPlink/sounds/148645/)
- [discomforting_harmony1.ogg](https://freesound.org/people/xxamoney27xx/sounds/630616/)
- [discomforting_harmony2.ogg](https://freesound.org/people/xxamoney27xx/sounds/630620/)
- [fizz.ogg](https://freesound.org/people/Cristian.Vogel/sounds/467876/)
- [slip.ogg](https://freesound.org/people/MATRIXXX_/sounds/495646/)
- [woosh.ogg](https://freesound.org/people/gerainsan/sounds/392092/)
