<% set item = items.challenge_horn %>
summon item ~.5 ~ ~.5 {Item:{Count:1,id:"minecraft:<<item.id>>",tag:<<item.nbt|dump_nbt>>}}

# sounds/effects on loot
particle minecraft:totem_of_undying ~0.5 ~0.5 ~0.5 0 0.5 0 0.3 20
playsound minecraft:entity.experience_orb.pickup block @a ~ ~ ~
playsound minecraft:item.axe.scrape block @a ~ ~ ~
playsound minecraft:entity.allay.death block @a ~ ~ ~
playsound minecraft:block.bell.resonate block @a ~ ~ ~
