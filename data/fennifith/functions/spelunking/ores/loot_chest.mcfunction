# generate a chest (if it hasn't already been created)
<% for table in ["surface", "intermediate", "special"] %>
<% set table_name %>fennifith:spelunking/loot_<<table>><% endset %>
execute if score #loot_type spl.args matches <<loop.index0>> if block ~ ~ ~ minecraft:trapped_chest run loot insert ~ ~ ~ loot <<table_name>>
execute if score #loot_type spl.args matches <<loop.index0>> unless block ~ ~ ~ minecraft:trapped_chest run setblock ~ ~ ~ minecraft:trapped_chest{CustomName:'{"text":"Loot Chest"}',LootTable:"<<table_name>>"}
<% endfor %>

# if the chest should have >1 loot, run this function again
scoreboard players remove #loot spl.args 1
execute if score #loot spl.args matches 1.. run function fennifith:spelunking/ores/loot_chest
particle minecraft:firework ~0.5 ~0.5 ~0.5 0 0.5 0 0.2 10
playsound minecraft:block.amethyst_block.step block @a ~ ~ ~
playsound minecraft:block.amethyst_block.step block @a ~ ~ ~
