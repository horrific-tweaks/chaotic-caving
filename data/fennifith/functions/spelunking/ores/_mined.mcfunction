---
collection: "ores"
filename: "mined_<< key >>.mcfunction"
---

# raycast to locate mined block position
function fennifith:spelunking/raycast/locate_oredrop

<% if item.sound %>
<% set pitches = ["0.8", "0.9", "1", "1.1", "1.2"] %>
execute store result score #random spl.tmp run data get entity @e[tag=spl.mined,limit=1] UUID[0] 1
scoreboard players set #random_max spl.tmp <<pitches|length>>
scoreboard players operation #random spl.tmp %= #random_max spl.tmp
<% for pitch in pitches %>
execute if score #random spl.tmp matches <<loop.index0>> at @e[tag=spl.mined,limit=1] run playsound <<item.sound>> block @a ~0.5 ~0.5 ~0.5 0.4 <<pitch>>
<% endfor %>
<% endif %>

# store whether a function has executed yet
scoreboard players set #run spl.args 0

<% if item.functions %>
<% for function in item.functions %>
# set function arguments
<% for key, value in function %>
<% if key != "id" and key != "random" %>
scoreboard players set #<<key>> spl.args << value >>
<% endif %>
<% endfor %>

<% if function.random %>
execute store result score #random spl.args run loot spawn ~ -65 ~ loot fennifith:spelunking/random_<< function.random | replace('%', '') >>
execute positioned ~ -65 ~ run kill @e[type=item,distance=..1,nbt={Item:{id:"minecraft:dirt"}}]

<% if function.random_limit %>
# add 1 to blocks mined since loot obtained
scoreboard players add @s spl.mined_since_loot 1
execute if score #random spl.args matches 1 run scoreboard players set @s spl.mined_since_loot 0

# guarantee one activation of this function whenever >=random_limit is reached
execute if entity @s[scores={spl.mined_since_loot=<<function.random_limit>>..}] run scoreboard players set #random spl.args 1
execute if entity @s[scores={spl.mined_since_loot=<<function.random_limit>>..}] run scoreboard players set @s spl.mined_since_loot 0
<% endif %>

<% if function.exclusive %>
# if this function is exclusive and another has already been executed, don't run this one
execute if score #run spl.args matches 1 run scoreboard players set #random spl.args 0
<% endif %>

# if random score is valid & surface is air, run ore functions
execute if score #random spl.args matches 1 run scoreboard players set #run spl.args 1
execute if score #random spl.args matches 1 at @e[tag=spl.mined,limit=1] if block ~ ~ ~ #fennifith:spelunking/raycast_ignore run function fennifith:spelunking/<< function.id >>
<% else %>
execute at @e[tag=spl.mined,limit=1] if block ~ ~ ~ #fennifith:spelunking/raycast_ignore run function fennifith:spelunking/<< function.id >>
<% endif %>

# reset argument values
<% for key, value in function %>
<% if key != "id" and key != "random" %>
scoreboard players reset #<<key>> spl.args
<% endif %>
<% endfor %>

<% endfor %>
<% endif %>

# clean up markers from function
kill @e[tag=spl.mined]
