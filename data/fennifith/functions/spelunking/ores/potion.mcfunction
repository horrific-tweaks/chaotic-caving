# some ores will spawn a potion
execute store result score #random_spawn spl.args run loot spawn ~ -65 ~ loot fennifith:spelunking/random_5
execute positioned ~ -65 ~ run kill @e[type=item,distance=..1,nbt={Item:{id:"minecraft:dirt"}}]

<% set potions_good = [
	"swiftness",
	"healing",
	"strength",
	"turtle_master",
	"regeneration",
	"night_vision"
] %>

<% set potions_bad %>
{Duration:900,Id:2b,Amplifier:2b}<# slowness #>
{Duration:900,Id:20b,Amplifier:0b}<# wither #>
<% endset %>
<% set potions_bad = potions_bad | trim %>
<% set potions_bad = potions_bad.split('\n') %>

<% for potion in potions_good %>
execute if score #random_spawn spl.args matches << loop.index0 >> run summon item ~0.5 ~0.5 ~0.5 {Item:{id:"minecraft:splash_potion",Count:1,tag:{Potion:"minecraft:<<potion>>"}}}
<% endfor %>

<% for potion in potions_bad %>
<% set summon %>
summon minecraft:area_effect_cloud ~ ~ ~ {Color:5793289,Radius:1.8f,Duration:120,RadiusOnUse:-0.01f,RadiusPerTick:-0.005f,Effects:[<<potion>>]}
<% endset %>
execute if score #random_spawn spl.args matches << potions_good|length + loop.index0 >> positioned ~0.5 ~ ~0.5 run << summon|trim >>
execute if score #random_spawn spl.args matches << potions_good|length + loop.index0 >> positioned ~0.5 ~1 ~0.5 run << summon|trim >>
execute if score #random_spawn spl.args matches << potions_good|length + loop.index0 >> positioned ~0.5 ~-1 ~0.5 run << summon|trim >>
<% endfor %>
