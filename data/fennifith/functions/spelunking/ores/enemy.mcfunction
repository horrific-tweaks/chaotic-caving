<% set enemies %>
summon zombie ~ ~ ~ {IsBaby:1,Health:5}
summon hoglin ~ ~ ~ {Age:-100000}
summon zoglin ~ ~ ~ {IsBaby:1}
summon magma_cube ~ ~ ~ {Size:0}
summon evoker_fangs
summon silverfish
summon shulker
summon bee ~ ~ ~ {Tags:["spl.bee"]}
summon chicken
summon cod
summon tnt ~ ~ ~ {Fuse:80}
summon cave_spider
<% endset %>
<% set enemies = enemies | trim %>
<% set enemies = enemies.split('\n') %>

# pick "random" enemy type to spawn
scoreboard players add #random_enemy spl.args 1
execute if score #random_enemy spl.args matches << enemies|length + 4 >>.. run scoreboard players set #random_enemy spl.args 0
#execute store result score #random_enemy spl.args run loot spawn ~ -65 ~ loot fennifith:spelunking/enemy
#execute positioned ~ -65 ~ run kill @e[type=item,distance=..1,nbt={Item:{id:"minecraft:dirt"}}]

<% for enemy in enemies %>
execute if score #random_enemy spl.args matches << loop.index0 >> positioned ~0.5 ~ ~0.5 run << enemy >>

<% set type = enemy.split(" ")[1] %>
# max lifetime = 5 mins
execute if score #random_enemy spl.args matches << loop.index0 >> run scoreboard players set @e[type=<<type>>,distance=..1] spl.age 5
execute if score #random_enemy spl.args matches << loop.index0 >> run tag @e[type=<<type>>,distance=..1] add spl.age

<% if enemy.includes("tnt") %>
execute if score #random_enemy spl.args matches << loop.index0 >> run playsound entity.tnt.primed block @a ~ ~ ~
<% endif %>
<% endfor %>

# summon vexes (only for special loot)
execute if score #enemies_hard spl.args matches 1 if score #random_enemy spl.args matches << enemies | length >> positioned ~0.5 ~ ~0.5 run summon vex
# extra vex for leading players
execute if entity @s[tag=spl.leading] if score #enemies spl.args matches 2.. if score #random_enemy spl.args matches << enemies | length >>..<< enemies | length + 1 >> positioned ~0.5 ~ ~0.5 run summon vex
# killer bunny for leading players
execute if entity @s[tag=spl.leading] if score #enemies spl.args matches 2.. if score #random_enemy spl.args matches << enemies | length + 2 >> run summon rabbit ~0.5 ~ ~0.5 {RabbitType:99}

# play summon noise if entity created
execute if score #random_enemy spl.args matches ..<< enemies|length - 1 >> run playsound minecraft:entity.evoker.prepare_summon block @a ~ ~ ~ 0.5

# 10% chance of a summoned bee being angry
execute store result score #random_anger spl.args run loot spawn ~ -65 ~ loot fennifith:spelunking/random_10
execute positioned ~ -65 ~ run kill @e[type=item,distance=..1,nbt={Item:{id:"minecraft:dirt"}}]
execute if score #random_enemy spl.args matches 1 run data modify entity @e[tag=spl.bee,limit=1] AngerTime set value 999999
tag @e[tag=spl.bee] remove spl.bee

# restart despawn loop if stopped
schedule function fennifith:spelunking/events/loop_despawn 60s append
