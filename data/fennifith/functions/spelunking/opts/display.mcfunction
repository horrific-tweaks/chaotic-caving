# Update option settings
<% for opt, choices in options %>
<% for choice, choiceinfo in choices %>
<% set choice_key = loop.index0 %>
# If choice is the default option and option is unset, apply value
<% if choiceinfo.default %>
execute unless score #opt_<<opt>> spl.global matches 0.. run scoreboard players set #opt_<<opt>> spl.global <<choice_key>>
<% endif %>
# If choice is active, run commands to enable
<% for command in choiceinfo.commands %>
execute if score #opt_<<opt>> spl.global matches <<choice_key>> in fennifith:spelunking run <<command>>
<% endfor %>
<% endfor %>
<% endfor %>

tellraw @a ["Game Options:"]

<% for opt, choices in options %>
<% for choice, choiceinfo in choices %>

<% set choices_disp %>
<% for c, _ in choices %>
<% set c_disp = c|replace('_',' ')|title %>
{
	"text":"<% if choice == c %>[<<c_disp>>]<% else %> <<c_disp>> <% endif %>",
	"color":"<< "green" if choice == c else "gray" >>",
	"clickEvent":{"action":"run_command","value":"/function fennifith:spelunking/opts/opt_<<opt>>"}
}
<% if not loop.last %>,<% endif %>
<% endfor %>
<% endset %>

execute if score #opt_<<opt>> spl.global matches <<loop.index0>> run tellraw @a ["<<opt|replace('_',' ')|title>> ",<<choices_disp|replace('\n','')|replace('\t','')|trim>>]
<% endfor %>
<% endfor %>
