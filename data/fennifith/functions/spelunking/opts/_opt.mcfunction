---
collection: options
filename: 'opt_<<key>>.mcfunction'
---

# cycle through the options for <<key>>
scoreboard players add #opt_<<key>> spl.global 1
scoreboard players set #length spl.tmp <<item|length>>
scoreboard players operation #opt_<<key>> spl.global %= #length spl.tmp

# re-display the menu to all users
function fennifith:spelunking/opts/display

<% for choice, choiceinfo in item %>
<% set choice_key = loop.index0 %>
# If choice is active, run commands to enable
<% for command in choiceinfo.activate %>
execute if score #opt_<<key>> spl.global matches <<choice_key>> in fennifith:spelunking run <<command>>
<% endfor %>
<% endfor %>
