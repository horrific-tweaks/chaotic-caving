# Heal exactly 0.5 hearts of health
effect give @s minecraft:regeneration 1 3 true

# Create particles
execute if entity @s[scores={spl.health=..19}] anchored eyes positioned ^ ^ ^2 run particle minecraft:crimson_spore ~ ~ ~ 0.5 0.5 0.5 0 50
execute if entity @s[scores={spl.health=..19}] run playsound fennifith:spelunking.woosh player @a ~ ~ ~

<% for i in ["1 0 0", "-1 0 0", "0 0 1", "0 0 -1"] %>
execute if entity @s[scores={spl.health=..19}] anchored eyes positioned ^ ^ ^2 run particle minecraft:damage_indicator ~ ~ ~ <<i>> 1 0 force
<% endfor %>

# Reset the crit detection advancement
advancement revoke @s only fennifith:spelunking/use_sword_leech
