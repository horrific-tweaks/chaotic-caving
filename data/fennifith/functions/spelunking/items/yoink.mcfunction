<% set item = items.yoink_stick %>

# get random number from 0-9
execute store result score #random spl.args run loot spawn ~ -65 ~ loot fennifith:spelunking/inventory_hotbar
execute positioned ~ -65 ~ run kill @e[type=item,distance=..1,nbt={Item:{id:"minecraft:dirt"}}]

# get current damage of the yoink stick
execute store result score #damage spl.args run data get entity @s SelectedItem.tag.Damage

# replace player's SelectedItem with air (to consume the yoink stick)
item replace entity @s weapon.mainhand with air

# remove yoink tags
tag @a[tag=spl.yoink_candidate] remove spl.yoink_candidate
tag @a[tag=spl.yoink] remove spl.yoink
tag @a[tag=spl.yoinker] remove spl.yoinker

tag @s add spl.yoinker

# select yoink candidates from other teams
<% for team, teaminfo in teams %>
execute if entity @s[team=spl.<<team>>] run tag @a[team=!spl.<<team>>] add spl.yoink_candidate
<% endfor %>

<% for slot in range(0, 9) %>
#   select a random player that has an item in the slot
execute if score #random spl.args matches <<slot>> as @a[tag=spl.yoink_candidate,sort=random,limit=1] run tag @s add spl.yoink
#   copy the selected slot from the yoinked player
execute if score #random spl.args matches <<slot>> if entity @a[tag=spl.yoink] run item replace entity @s weapon.mainhand from entity @a[tag=spl.yoink,limit=1] hotbar.<<slot>>
#   store yoink stick in other player's copied slot
execute if score #random spl.args matches <<slot>> if entity @a[tag=spl.yoink] run item replace entity @a[tag=spl.yoink,limit=1] hotbar.<<slot>> with <<item.id>><<item.nbt|dump_nbt>>
#   damage the new yoink stick according to previous damage value
execute if score #random spl.args matches <<slot>> if entity @a[tag=spl.yoink] run item modify entity @a[tag=spl.yoink,limit=1] hotbar.<<slot>> fennifith:spelunking/damage_4
execute if score #random spl.args matches <<slot>> if entity @a[tag=spl.yoink] if score #damage spl.args matches 1.. run item modify entity @a[tag=spl.yoink,limit=1] hotbar.<<slot>> fennifith:spelunking/damage_4
execute if score #random spl.args matches <<slot>> if entity @a[tag=spl.yoink] if score #damage spl.args matches 10.. run item replace entity @a[tag=spl.yoink,limit=1] hotbar.<<slot>> with air
<% endfor %>

# play cast_spell noise to players
playsound minecraft:entity.illusioner.cast_spell player @a ~ ~ ~
execute as @a[tag=spl.yoink] at @s run playsound minecraft:entity.illusioner.cast_spell player @a ~ ~ ~
execute as @a[tag=spl.yoink] at @s run playsound fennifith:spelunking.slip player @a ~ ~ ~

# if the stick breaks...
execute if entity @a[tag=spl.yoink] if score #damage spl.args matches 10.. run playsound entity.item.break player @a ~ ~ ~
execute if entity @a[tag=spl.yoink] if score #damage spl.args matches 10.. as @a[tag=spl.yoink] at @s run playsound entity.item.break player @a ~ ~ ~

# summon a temporary item entity with the yoinked item data
execute if entity @a[tag=spl.yoink] if entity @s[nbt={SelectedItem:{}}] run summon item ~ -65 ~ {Tags:["spl.yoinked"],Item:{id:"minecraft:dirt",Count:1b}}
execute if entity @a[tag=spl.yoink] run data modify entity @e[type=item,tag=spl.yoinked,limit=1] Item set from entity @s SelectedItem

# tell players what happened
#   if the yoink stick works
#     if the item has a custom display name
execute if entity @e[type=item,tag=spl.yoinked,nbt={Item:{tag:{display:{}}}}] run tellraw @a [{"selector":"@s","color":"aqua"},{"text":" used a Yoink Stick to steal ","color":"white"},{"nbt":"Item.tag.display.Name","entity":"@e[type=item,tag=spl.yoinked,limit=1]","interpret":true},{"text":" from ","color":"white"},{"selector":"@a[tag=spl.yoink]","color":"aqua"},{"text":"!","color":"white"}]
#     if the item needs a supplied name
execute if entity @e[type=item,tag=spl.yoinked,nbt=!{Item:{tag:{display:{}}}}] run tellraw @a [{"selector":"@s","color":"aqua"},{"text":" used a Yoink Stick to steal ","color":"white"},{"selector":"@e[type=item,tag=spl.yoinked,limit=1]","color":"yellow"},{"text":" from ","color":"white"},{"selector":"@a[tag=spl.yoink]","color":"aqua"},{"text":"!","color":"white"}]
#     if the item is an empty slot
execute if entity @a[tag=spl.yoink] unless entity @e[type=item,tag=spl.yoinked] run tellraw @a [{"selector":"@s","color":"aqua"},{"text":" used a Yoink Stick to steal ","color":"white"},{"text":"Air","color":"yellow"},{"text":" from ","color":"white"},{"selector":"@a[tag=spl.yoink]","color":"aqua"},{"text":"!","color":"white"}]
#   if the yoink stick fails (+ the yoink stick did not break)
execute unless entity @a[tag=spl.yoink] if score #damage spl.args matches 10.. run playsound entity.item.break player @a ~ ~ ~

kill @e[type=item,tag=spl.yoinked]

# reset yoink tag
#tag @a[tag=spl.yoink_candidate] remove spl.yoink_candidate
#tag @a[tag=spl.yoink] remove spl.yoink
#tag @a[tag=spl.yoinker] remove spl.yoinker
