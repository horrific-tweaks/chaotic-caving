<% set item = items.mjolnir %>
<% set item_query %>id:"minecraft:<<item.id>>",tag:{CustomModelData:<<item.nbt.CustomModelData>>}<% endset %>
# clear the used item
execute as @s[nbt={Inventory:[{Slot:-106b,<<item_query>>}]},nbt=!{SelectedItem:{<<item_query>>}}] run item replace entity @s weapon.offhand with air
execute as @s[nbt={SelectedItem:{<<item_query>>}}] run item replace entity @s weapon.mainhand with air

# summon lightning bolts
execute anchored eyes positioned ^ ^ ^4 run summon minecraft:lightning_bolt
execute anchored eyes positioned ^ ^ ^6 run summon minecraft:lightning_bolt
execute anchored eyes positioned ^ ^ ^8 run summon minecraft:lightning_bolt
execute anchored eyes positioned ^ ^ ^10 run summon minecraft:lightning_bolt
execute anchored eyes positioned ^ ^ ^12 run summon minecraft:lightning_bolt
