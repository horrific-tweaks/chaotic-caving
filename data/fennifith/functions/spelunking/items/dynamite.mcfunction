<% set item = items.dynamite %>
<% set item_tag = item.nbt.EntityTag.Tags[0] %>

# Replace item frame with axis-aligned position marker
kill @e[type=!<<item.id>>,tag=<<item_tag>>]
execute at @e[type=<<item.id>>,tag=<<item_tag>>,limit=1] align xyz run summon marker ~ ~ ~ {Tags:["<<item_tag>>"]}
kill @e[type=<<item.id>>,tag=<<item_tag>>,limit=1]

# give players resistance effect
execute at @e[tag=<<item_tag>>,limit=1] run effect give @a[distance=..5] minecraft:resistance 1 10 true

# summon explosion
execute at @e[tag=<<item_tag>>,limit=1] run summon minecraft:fireball ~0.5 ~0.5 ~0.5 {ExplosionPower:3,power:[0.0,-0.5,0.0],CustomName:'"volatile magic"'}

# Remove the position marker entity
kill @e[tag=<<item_tag>>,limit=1]
