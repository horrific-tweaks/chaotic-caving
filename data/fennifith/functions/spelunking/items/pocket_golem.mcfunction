<% set item = items.pocket_golem %>
<% set item_tag = item.nbt.EntityTag.Tags[0] %>

# Replace item frame with axis-aligned position marker
kill @e[type=!<<item.id>>,tag=<<item_tag>>]
execute at @e[type=<<item.id>>,tag=<<item_tag>>,limit=1] align xyz run summon marker ~ ~ ~ {Tags:["<<item_tag>>"]}
kill @e[type=<<item.id>>,tag=<<item_tag>>,limit=1]

# Check that spawn location is clear
<% set conditions %>
if block ~ ~ ~ #fennifith:spelunking/raycast_ignore
if block ~ ~1 ~ #fennifith:spelunking/raycast_ignore
if block ~ ~2 ~ #fennifith:spelunking/raycast_ignore
if block ~ ~2 ~1 #fennifith:spelunking/raycast_ignore
if block ~ ~2 ~-1 #fennifith:spelunking/raycast_ignore
if block ~1 ~2 ~ #fennifith:spelunking/raycast_ignore
if block ~1 ~2 ~1 #fennifith:spelunking/raycast_ignore
if block ~1 ~2 ~-1 #fennifith:spelunking/raycast_ignore
if block ~-1 ~2 ~ #fennifith:spelunking/raycast_ignore
if block ~-1 ~2 ~1 #fennifith:spelunking/raycast_ignore
if block ~-1 ~2 ~-1 #fennifith:spelunking/raycast_ignore
if entity @e[tag=<<item_tag>>,limit=1]
<% endset %>
<% set conditions = conditions | trim %>
<% set conditions = conditions.split('\n') %>

# if any of the conditions passes (i.e. the spawn is not valid), #valid_spawn = <<expected>>
scoreboard players set #valid spl.args 0
<% set expected = 0 %>

<% for condition in conditions | batch(4) %>
execute if score #valid spl.args matches << loop.index - 1 >> at @e[tag=<<item_tag>>,limit=1] << condition | join(' ') >> run scoreboard players set #valid spl.args << loop.index >>
<% set expected = loop.index %>
<% endfor %>

# if the spawn is valid, summon a golem
execute at @e[tag=<<item_tag>>,limit=1] if score #valid spl.args matches << expected >> run summon iron_golem ~ ~ ~ {PlayerCreated:1b}
execute at @e[tag=<<item_tag>>,limit=1] if score #valid spl.args matches << expected >> run particle minecraft:cloud ~ ~ ~ 0.7 0.7 0.7 0 16

# if the spawn is not valid, give the player item back
execute unless score #valid spl.args matches << expected >> run title @s actionbar "There isn't enough space to use this here"
execute unless score #valid spl.args matches << expected >> at @e[tag=<<item_tag>>,limit=1] run summon item ~.5 ~.5 ~.5 {Item:{Count:1,id:"<< item.id >>",tag:<< item.nbt | dump_nbt >>}}

# Remove the position marker entity
kill @e[tag=<<item_tag>>,limit=1]
