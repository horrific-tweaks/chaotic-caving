<% set item = items.chaos_crystal %>

# clear the used item from the player
execute unless score #lock_powerups spl.args matches 1 run clear @s <<item.id>>{CustomModelData:<<item.nbt.CustomModelData>>} 1

# activate the random effect
playsound minecraft:block.respawn_anchor.deplete player @s
execute unless score #lock_powerups spl.args matches 1 run function fennifith:spelunking/powerups/effect
