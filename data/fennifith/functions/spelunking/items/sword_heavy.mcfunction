# create particles
execute anchored eyes positioned ^ ^ ^2 run particle minecraft:explosion ~ ~ ~ .2 1 .2 0 2 force

# play sounds
playsound minecraft:entity.warden.attack_impact player @a
playsound minecraft:entity.ravager.step player @s

# Reset the detection advancement
advancement revoke @s only fennifith:spelunking/use_sword_heavy
