# Teleport user to the surface
spreadplayers ~ ~ 0 1 false @s
execute if block ~ 62 ~ water if block ~ 63 ~ air if block ~ 64 ~ air run tp @s ~ 63 ~

# Play chorus fruit sound
playsound item.chorus_fruit.teleport player @a ~ ~ ~
particle reverse_portal ~ ~ ~ 1 1 1 0 30

# Clear empty bowl
tag @s add spl.fix_bowl
schedule function fennifith:spelunking/items/fix_bowl 1t replace
