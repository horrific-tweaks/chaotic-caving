<% set item = items.challenge_horn %>
<% set item_query %>id:"minecraft:<<item.id>>",tag:{CustomModelData:<<item.nbt.CustomModelData>>}<% endset %>
# clear the used item
execute unless score #locked spl.rift matches 1 as @s[nbt={Inventory:[{Slot:-106b,<<item_query>>}]},nbt=!{SelectedItem:{<<item_query>>}}] run item replace entity @s weapon.offhand with air
execute unless score #locked spl.rift matches 1 as @s[nbt={SelectedItem:{<<item_query>>}}] run item replace entity @s weapon.mainhand with air

# make goat horn noise
execute unless score #locked spl.rift matches 1 run playsound minecraft:item.goat_horn.sound.6 player @a ~ ~ ~ 16.0 1.0

# only start one encounter at a time
execute unless score #locked spl.rift matches 1 run function fennifith:spelunking/encounter/rift_start
