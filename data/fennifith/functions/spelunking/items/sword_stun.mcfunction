<% set selector = "@e[type=!#fennifith:spelunking/electric_immune,distance=..2]" %>

# Apply slowness to enemies within "swing radius"
execute anchored eyes positioned ^ ^ ^3 run effect give << selector >> minecraft:slowness 2 100

# Create particles
execute anchored eyes positioned ^ ^ ^3 at << selector >> run particle minecraft:underwater ~ ~0.5 ~ 0.5 0.5 0.5 0 50
playsound fennifith:spelunking.fizz player @a ~ ~ ~

# Reset the crit detection advancement
advancement revoke @s only fennifith:spelunking/use_sword_stun
