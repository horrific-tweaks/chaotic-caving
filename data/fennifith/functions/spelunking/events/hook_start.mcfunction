# set global running flag
scoreboard players set #global_running spl.args 1

function fennifith:spelunking/events/load
execute in fennifith:spelunking run forceload add 0 0

# change gamemode to spectator
gamemode spectator @a
tag @a add spl.spectator
# place players in dimension
execute in fennifith:spelunking run tp @a <<config.pos_info>>
#   spreadplayers to starting structure
#   spreadplayers 0 0 1 6 true @a
#   if the structure entity is found, try to place everyone there
#   execute at @e[type=armor_stand,tag=spl.return,limit=1] run spreadplayers ~ ~ 1 6 true @a

# show config menu
execute as @p run function fennifith:spelunking/opts/display

# summon info item
kill @e[type=armor_stand,tag=spl.info]
execute positioned <<config.pos_info>> run summon armor_stand ~ ~-10 ~ {Tags:["spl.info"],DisabledSlots:4144959,Invulnerable:1b,NoGravity:1b,Invisible:1b}
item replace entity @e[type=minecraft:armor_stand,tag=spl.info] armor.head with minecraft:stick{CustomModelData:817334}

# start lobby music loop
schedule function fennifith:spelunking/events/loop_music 10s replace

# say DEBUG: start caving