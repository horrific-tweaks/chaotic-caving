# set global running flag
scoreboard players set #global_running spl.args 0

# stop the game, if running
function fennifith:spelunking/timer/on_end

# remove all player tags
tag @a remove spl.player

# reset game options
<% for opt, choices in options %>
<% for choice, choiceinfo in choices %>
# if choice is the default option, set scoreboard value to match
<% if choiceinfo.default %>
scoreboard players set #opt_<<opt>> spl.global <<loop.index0>>
<% endif %>
<% endfor %>
<% endfor %>

# remove player teams
<% for team, teaminfo in teams %>
team remove spl.<<team>>
<% endfor %>

# say DEBUG: spelunking reset
