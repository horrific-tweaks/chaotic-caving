execute if entity @s[tag=min.sign] unless entity @e[tag=spl.sign] run tag @s add spl.sign
execute if entity @s[tag=spl.sign] run tag @s remove min.sign
execute if entity @s[tag=spl.sign] at @s run data modify block ~ ~ ~ front_text.messages[1] set value '{"text":"Chaotic","clickEvent":{"action":"run_command","value":"execute unless score #global_stop min.args matches 0 run function fennifith:spelunking/events/hook_start"}}'
execute if entity @s[tag=spl.sign] at @s run data modify block ~ ~ ~ front_text.messages[2] set value '{"text":"Caving"}'
execute if entity @s[tag=spl.sign] at @s run setblock ~ ~3 ~ diamond_ore
