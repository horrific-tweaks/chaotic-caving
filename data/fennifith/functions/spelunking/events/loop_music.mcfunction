# if the game is playing and has not yet finished...
execute in fennifith:spelunking if score #global_running spl.args matches 1 if score #timer spl.args matches 1 run function fennifith:spelunking/events/loop_music_game

# if the game is over...
execute in fennifith:spelunking if score #global_running spl.args matches 1 unless score #timer spl.args matches 1 run function fennifith:spelunking/events/loop_music_lobby

# run once every 4 minutes
execute if score #global_running spl.args matches 1 run schedule function fennifith:spelunking/events/loop_music 240s replace
