# place info item/map on armor stand
item replace entity @e[type=minecraft:armor_stand,tag=spl.info] armor.head with minecraft:stick{CustomModelData:817334}

# set coordinates for portal return point
execute as @e[type=armor_stand,tag=spl.portal,limit=1] run data modify storage spelunking:global portal set from entity @s Pos
execute as @e[type=armor_stand,tag=spl.portal,limit=1] at @s run forceload add ~ ~

# if non-players stray too far from the info item, teleport them back
execute in fennifith:spelunking positioned <<config.pos_info>> as @a[tag=spl.spectator,distance=50..] run tp @s <<config.pos_info>>

# run once every 10 seconds
execute if score #global_running spl.args matches 1 run schedule function fennifith:spelunking/events/loop_find_structures 10s replace
