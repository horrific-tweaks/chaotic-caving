# Get player's current y position
execute as @a store result score @s spl.tmp run data get entity @s Pos[1] 1

# For anyone where y<=50, play an ambient noise...
execute as @a[scores={spl.tmp=..50}] at @s run playsound fennifith:spelunking.ambience ambient @s ~ ~ ~ 300

# run once every 2 minutes
execute if score #global_running spl.args matches 1 run schedule function fennifith:spelunking/events/loop_ambience 180s replace