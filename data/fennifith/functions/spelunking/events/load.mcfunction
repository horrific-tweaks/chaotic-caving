scoreboard objectives add min.args dummy

scoreboard objectives add spl.tmp dummy
scoreboard objectives add spl.global dummy
scoreboard objectives add spl.cast dummy
scoreboard objectives add spl.args dummy
scoreboard objectives add spl.age dummy

scoreboard objectives add spl.mined_since_loot dummy

scoreboard objectives add spl.rift dummy

# add objectives for block tracking
<% for block, info in ores %>
scoreboard objectives add spl.<< block >> minecraft.mined:minecraft.<< block >>
scoreboard players reset * spl.<< block >>
<% endfor %>

<% for item, info in items %>
<% if info.functions %>
scoreboard objectives add spl.<< item >> minecraft.used:minecraft.<< info.id >>
scoreboard players reset * spl.<< item >>
<% endif %>
<% endfor %>

scoreboard objectives add spl.health health

scoreboard objectives add spl.deaths deathCount
scoreboard players reset * spl.deaths

scoreboard objectives add spl.sneaking minecraft.custom:minecraft.sneak_time

# set up time & gamerules
execute in fennifith:spelunking run difficulty normal
execute in fennifith:spelunking run time set night
execute in fennifith:spelunking run gamerule doPatrolSpawning true
execute in fennifith:spelunking run gamerule doMobSpawning true
execute in fennifith:spelunking run gamerule doMobLoot true
execute in fennifith:spelunking run gamerule mobGriefing true
execute in fennifith:spelunking run gamerule doDaylightCycle false
execute in fennifith:spelunking run gamerule doWeatherCycle false
execute in fennifith:spelunking run gamerule doImmediateRespawn false
execute in fennifith:spelunking run gamerule keepInventory true
execute in fennifith:spelunking run gamerule announceAdvancements false
execute in fennifith:spelunking run gamerule playersSleepingPercentage 200
execute in fennifith:spelunking run gamerule doInsomnia false
execute in fennifith:spelunking run gamerule commandBlockOutput false

# add player teams
<% for team, teaminfo in teams %>
#   add the team entry
team add spl.<<team>> "<<team>>"
#   set team attributes
team modify spl.<<team>> color <<teaminfo.color>>
team join spl.<<team>> <<team>>
<% endfor %>

# track xp per-player
scoreboard objectives add spl.xp xp
xp set @a 0 levels
xp set @a 0 points
xp add @a -1 points
scoreboard players reset * spl.xp

# create points sidebar
scoreboard objectives add spl.points dummy "Chaos Shards"
scoreboard players reset * spl.points

# start entity despawn loop
schedule function fennifith:spelunking/events/loop_despawn 60s replace
schedule function fennifith:spelunking/events/loop_ambience 60s replace
schedule function fennifith:spelunking/events/loop_effects 2s replace
schedule function fennifith:spelunking/events/loop_find_structures 10s replace
