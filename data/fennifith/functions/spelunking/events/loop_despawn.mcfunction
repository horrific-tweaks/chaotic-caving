# entity age/cleanup ---
#   decrease lifetime of spawned mobs
execute as @e[tag=spl.age] run scoreboard players remove @s spl.age 1
#   if mob reached 0 age, tp into void to despawn
execute as @e[tag=spl.age,scores={spl.age=..0}] at @s run particle minecraft:cloud ~ ~0.5 ~ 1 1 1 0 10
execute as @e[tag=spl.age,scores={spl.age=..0}] at @s run playsound entity.generic.burn hostile @a ~ ~ ~ 0.5
execute as @e[tag=spl.age,scores={spl.age=..0}] at @s run tp @s 0 -256 0

# run once per minute
execute if score #global_running spl.args matches 1 run schedule function fennifith:spelunking/events/loop_despawn 60s replace
