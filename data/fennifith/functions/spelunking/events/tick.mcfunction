# when an ore is mined, invoke its function
<% for block, info in ores %>
# if block has been mined in the last tick, scoreboard != 0
execute as @a unless score @s spl.<< block >> matches 0 at @s run function fennifith:spelunking/ores/mined_<< block >>
# reset scoreboard after adding points
scoreboard players set @a spl.<< block >> 0
<% endfor %>

# when an item is used, invoke its function(s)
<% for item, info in items %>
<% if info.functions %>
<% for function in info.functions %>
execute as @a[tag=spl.player,scores={spl.<<item>>=1..},nbt={SelectedItem:{id:"minecraft:<<info.id>>",tag:{CustomModelData:<<info.nbt.CustomModelData>>}}}] at @s run function fennifith:spelunking/items/<<function>>
<% endfor %>
# reset item scoreboard
scoreboard players set @a spl.<< item >> 0
<% endif %>
<% endfor %>

# determine leading team player tags
# BUGGY CODE AAAAAAAAAAAA
tag @a remove spl.leading
scoreboard players set #leader spl.args 0
execute as @a run scoreboard players operation #leader spl.args > @s spl.xp
execute as @a if score #leader spl.args <= @s spl.xp run tag @s add spl.leading

# on player death:
execute as @a[scores={spl.deaths=1..}] in fennifith:spelunking run function fennifith:spelunking/points/on_death
scoreboard players reset @a spl.deaths

# if a player has respawned
execute as @a[tag=spl.player,nbt=!{Dimension:"fennifith:spelunking"}] unless entity @s[nbt={Dimension:"minecraft:the_nether"}] in fennifith:spelunking run function fennifith:spelunking/points/on_respawn
execute as @e[type=player,tag=spl.player,tag=spl.respawning] in fennifith:spelunking run function fennifith:spelunking/points/on_respawn
tag @e[type=player,tag=spl.player,tag=spl.respawning] remove spl.respawning

# if a player has entered a nether portal
execute as @a[tag=spl.player,nbt={Dimension:"minecraft:the_nether"}] in fennifith:spelunking run function fennifith:spelunking/portal/on_enter

# run timer tick
function fennifith:spelunking/timer/tick

# revoke all detection advancements
advancement revoke @a everything
