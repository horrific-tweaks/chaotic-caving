# re-caculate effects for active items
<% for id, item in items %>
<% for effect in item.effects %>
<% if effect.slot == "mainhand" %>
execute as @a[tag=spl.player,nbt={SelectedItem:{id:"minecraft:<<item.id>>",tag:{CustomModelData:<<item.nbt.CustomModelData>>}}}] run effect give @s <<effect.id>> <<effect.duration|default(1)>> <<effect.amplifier|default(1)>>
<% else %>
execute as @a[tag=spl.player,nbt={Inventory:[{Slot:<<effect.slot>>b,id:"minecraft:<<item.id>>",tag:{CustomModelData:<<item.nbt.CustomModelData>>}}]}] run effect give @s <<effect.id>> <<effect.duration|default(1)>> <<effect.amplifier|default(1)>>
<% endif %>
<% endfor %>
<% endfor %>

# reset sneaking status
scoreboard players reset @a[scores={spl.sneaking=1..}] spl.sneaking

# provide all players with compass
execute as @e[type=player,tag=spl.player] store result score @s spl.tmp run clear @s compass 0
give @e[type=player,tag=spl.player,scores={spl.tmp=0}] compass{CustomModelData:817330,display:{Name:'{"text":"Spawn Compass","italic":0,"color":"yellow"}'},LodestoneDimension:"fennifith:spelunking",LodestoneTracked:0b,LodestonePos:{X:0,Y:0,Z:0}}
kill @e[type=item,nbt={Item:{id:"minecraft:compass",tag:{CustomModelData:817330}}}]

# run once every 0.5 seconds
execute if score #global_running spl.args matches 1 run schedule function fennifith:spelunking/events/loop_effects 10t replace