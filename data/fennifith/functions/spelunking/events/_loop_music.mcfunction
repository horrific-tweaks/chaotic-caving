---
collection: "music"
filename: "loop_music_<<key>>.mcfunction"
---

# get a random track index from the loot table
execute store result score #random spl.tmp run loot spawn ~ -65 ~ loot fennifith:spelunking/music_<<key>>
# if the chosen track is equal to the last played, increment by 1
execute if score #track spl.args = #random spl.tmp run scoreboard players add #random spl.tmp 1
# modulus by number of tracks to prevent overflow
scoreboard players set #divisor spl.tmp <<item|length>>
scoreboard players operation #random spl.tmp %= #divisor spl.tmp
# copy result to actual track variable
scoreboard players operation #track spl.args = #random spl.tmp

# find the track sound to play
<% for track in item %>
execute in fennifith:spelunking if score #track spl.args matches <<loop.index0>> as @a at @s run playsound fennifith:spelunking_music.<<track>> music @s
<% endfor %>
