# Clean up markers from raycast
kill @e[type=marker,tag=spl.fire]

# Initialize and start raycasting.
scoreboard players set #hit spl.cast 0
scoreboard players set #distance spl.cast 0
execute as @s at @s anchored eyes positioned ^ ^ ^ anchored feet run function fennifith:spelunking/portal/raycast/ray
