# Executes in a loop until a given block has been found.
execute if block ~ ~ ~ minecraft:fire if block ~ ~-1 ~ minecraft:obsidian run function fennifith:spelunking/portal/raycast/hit_block
scoreboard players add #distance spl.cast 1
# if outer edge of ray length is reached, assume the block position is found
execute if score #hit spl.cast matches 0 unless score #distance spl.cast matches ..45 run function fennifith:spelunking/portal/raycast/hit_block
# otherwise, continue iterating
execute if score #hit spl.cast matches 0 if score #distance spl.cast matches ..45 positioned ^ ^ ^0.1 run function fennifith:spelunking/portal/raycast/ray
