# manually teleport to pyramid portal location
summon marker 0 0 0 {Tags:["spl.portal_tmp"]}
#   set marker position to global spawn (set in on_start)
execute as @e[type=marker,tag=spl.portal_tmp] run data modify entity @s Pos set from storage spelunking:global portal
#   teleport player to the marker upon respawn
execute at @e[type=marker,tag=spl.portal_tmp,limit=1] run setblock ~ ~ ~ air destroy
execute at @e[type=marker,tag=spl.portal_tmp,limit=1] run setblock ~ ~1 ~ air destroy
tp @s @e[type=marker,tag=spl.portal_tmp,limit=1]
# remove temp marker
kill @e[type=marker,tag=spl.portal_tmp]
