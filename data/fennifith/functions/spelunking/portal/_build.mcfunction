---
collection: portal.offsets
filename: "build_<<item[0]>>_<<item[1]>>_<<item[2]>>.mcfunction"
---

<% set rot = item[0] %>
<% set x = item[1] %>
<% set z = item[2] %>

say building portal!

setblock ~ ~ ~ minecraft:nether_portal[axis=<<rot>>]
setblock ~<<x*1>> ~ ~<<z*1>> minecraft:nether_portal[axis=<<rot>>]
setblock ~ ~1 ~ minecraft:nether_portal[axis=<<rot>>]
setblock ~<<x*1>> ~1 ~<<z*1>> minecraft:nether_portal[axis=<<rot>>]
setblock ~ ~2 ~ minecraft:nether_portal[axis=<<rot>>]
setblock ~<<x*1>> ~2 ~<<z*1>> minecraft:nether_portal[axis=<<rot>>]

# If a portal was lit with the flint_and_steel, break the item...
execute if entity @s[nbt={SelectedItem:{id:"minecraft:flint_and_steel"}}] at @s run playsound entity.item.break player @a
execute if entity @s[nbt={SelectedItem:{id:"minecraft:flint_and_steel"}}] run item replace entity @s weapon.mainhand with air
