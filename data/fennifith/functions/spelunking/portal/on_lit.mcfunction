# Raycast to the location of the fire created by flint_and_steel
function fennifith:spelunking/portal/raycast/start_ray

<% for rot, x, z in portal.offsets %>
<% set if_portal %>
if block ~ ~-1 ~ minecraft:obsidian
if block ~<<x*1>> ~-1 ~<<z*1>> minecraft:obsidian
if block ~ ~3 ~ minecraft:obsidian
if block ~<<x*1>> ~3 ~<<z*1>> minecraft:obsidian
if block ~<<x*-1>> ~ ~<<z*-1>> minecraft:obsidian
if block ~<<x*-1>> ~1 ~<<z*-1>> minecraft:obsidian
if block ~<<x*-1>> ~2 ~<<z*-1>> minecraft:obsidian
if block ~<<x*2>> ~ ~<<z*2>> minecraft:obsidian
if block ~<<x*2>> ~1 ~<<z*2>> minecraft:obsidian
if block ~<<x*2>> ~2 ~<<z*2>> minecraft:obsidian
<% endset %>

execute at @e[type=marker,tag=spl.fire] <<if_portal.trim().split('\n').join(" ")>> run function fennifith:spelunking/portal/build_<<rot>>_<<x>>_<<z>>
<% endfor %>

# Clean up markers from raycast
kill @e[type=marker,tag=spl.fire]
advancement revoke @s only fennifith:spelunking/flint_and_steel
