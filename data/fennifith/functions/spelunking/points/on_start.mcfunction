function fennifith:spelunking/aaa/reset

# grant recipes to all players
recipe give @a *
clear @a
kill @e[type=item]
kill @e[type=armor_stand,tag=spl.info]

# set scoreboard to display in sidebar of all teams
scoreboard objectives setdisplay sidebar spl.points
<% for team, teaminfo in teams %>
scoreboard objectives setdisplay sidebar.team.<<teaminfo.color>> spl.points
<% endfor %>

# distribute players between teams
<% for team, teaminfo in teams %>
#   add players to the team
<% for players in range(1, 4) %>
execute if score #opt_players_per_team spl.global matches <<players-1>> run team join spl.<<team>> @a[team=,limit=<<players>>]
<% endfor %>

#   set initial points value for each team
execute if entity @a[team=spl.<<team>>] run scoreboard players set <<team>> spl.points 0
<% endfor %>

# set players to adventure gamemode
gamemode adventure @a
tag @a remove spl.spectator

# place players in dimension
execute in fennifith:spelunking run tp @a 0 120 0
#   spreadplayers to starting structure
spreadplayers 0 0 1 6 true @a
#   if the structure entity is found, try to place everyone there
execute at @e[type=armor_stand,tag=spl.return,limit=1] run spreadplayers ~ ~ 1 6 true @a

# once teleported, set spawnpoints
execute as @a at @s run forceload add ~ ~
execute as @a at @s run setblock ~ ~ ~ air
execute as @a at @s run setblock ~ ~1 ~ air
execute as @a at @s run spawnpoint @s ~ ~ ~

# set global respawn coordinates
execute as @a[limit=1] run data modify storage spelunking:global spawn set from entity @s Pos

# stop any playing lobby music
<% for key, item in music %>
<% for track in item %>
execute in fennifith:spelunking run stopsound @a music fennifith:spelunking_music.<<track>>
<% endfor %>
<% endfor %>

# begin countdown
schedule function fennifith:spelunking/timer/countdown 20s replace
