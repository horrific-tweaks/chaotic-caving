# get max points score
scoreboard players set #leading spl.tmp 0
<% for team, teaminfo in teams %>
execute if entity @a[team=spl.<<team>>] run scoreboard players operation #leading spl.tmp > <<team>> spl.points
<% endfor %>

# play game completion effects
playsound ui.toast.challenge_complete player @a

<% for team, teaminfo in teams %>
# if team is the winner...
execute if score <<team>> spl.points >= #leading spl.tmp as @a[team=spl.<<team>>] at @s run playsound item.firecharge.use player @a
execute if score <<team>> spl.points >= #leading spl.tmp as @a[team=spl.<<team>>] at @s run playsound entity.firework_rocket.large_blast player @a
execute if score <<team>> spl.points >= #leading spl.tmp as @a[team=spl.<<team>>] at @s run particle firework ~ ~ ~ 0 0 0 0.5 50

execute if score <<team>> spl.points >= #leading spl.tmp run tellraw @a [{"text":"Game over - Team <<team>> is the winner!","color":"aqua"}]
<% endfor %>
