# manually teleport to team spawn
summon marker 0 0 0 {Tags:["spl.spawn_tmp"]}
#   set marker position to global spawn (set in on_start)
execute as @e[type=marker,tag=spl.spawn_tmp] run data modify entity @s Pos set from storage spelunking:global spawn
#   teleport player to the marker upon respawn
execute at @e[type=marker,tag=spl.spawn_tmp,limit=1] run setblock ~ ~ ~ air destroy
execute at @e[type=marker,tag=spl.spawn_tmp,limit=1] run setblock ~ ~1 ~ air destroy
tp @s @e[type=marker,tag=spl.spawn_tmp,limit=1]
#   re-apply player spawnpoint if teleported
execute if entity @e[type=marker,tag=spl.spawn_tmp] at @s[nbt={Dimension:"fennifith:spelunking"}] run spawnpoint @s ~ ~ ~
kill @e[type=marker,tag=spl.spawn_tmp]

# clear point shards from player
clear @s <<items.chaos_shard.id>>{CustomModelData:<<items.chaos_shard.nbt.CustomModelData>>}

# reset player's xp score
xp set @s 0 levels
xp set @s 0 points
xp add @s -1 points
scoreboard players reset @s spl.xp
