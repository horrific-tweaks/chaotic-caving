# create sculk to hold the player's points
summon marker ~ ~ ~ {Tags:["spl.death"]}

# set marker position to match player coords
data modify entity @e[type=marker,tag=spl.death,sort=nearest,limit=1] Pos set from entity @s Pos

# move marker to block below player (i.e. standing on)
execute as @e[tag=spl.death] at @s run tp @s ~ ~-1 ~

# calculate max xp value (ensure that it is below 500)
scoreboard players operation #xp spl.tmp = @s spl.xp
scoreboard players set #max spl.tmp 1000
scoreboard players operation #xp spl.tmp < #max spl.tmp

# create a constant sculk catalyst at 0,0
execute unless block 0 -64 0 sculk_catalyst run setblock 0 -64 0 sculk_catalyst
execute if score #xp spl.tmp matches 5.. at @e[tag=spl.death] if block ~ ~ ~ #minecraft:sculk_replaceable run setblock ~ ~ ~ sculk

# create a temporary charge data entry
data modify storage spelunking:on_death charge set value {decay_delay:100,update_delay:10,facings:["up"]}
# copy charge position from marker entity
data modify storage spelunking:on_death charge.pos set value [0,0,0]
execute store result storage spelunking:on_death charge.pos[0] int 1 run data get entity @e[type=marker,tag=spl.death,sort=nearest,limit=1] Pos[0]
execute store result storage spelunking:on_death charge.pos[1] int 1 run data get entity @e[type=marker,tag=spl.death,sort=nearest,limit=1] Pos[1]
execute store result storage spelunking:on_death charge.pos[2] int 1 run data get entity @e[type=marker,tag=spl.death,sort=nearest,limit=1] Pos[2]
# set scaled xp value
execute store result storage spelunking:on_death charge.charge int 0.2 run scoreboard players get #xp spl.tmp

# add charge to sculk block
data modify block 0 -64 0 cursors append from storage spelunking:on_death charge
kill @e[tag=spl.death]

# clear point shards from player
clear @s <<items.chaos_shard.id>>{CustomModelData:<<items.chaos_shard.nbt.CustomModelData>>}

# clear any items with curse of vanishing
clear @s #fennifith:spelunking/all{Enchantments:[{id:"minecraft:vanishing_curse"}]}

# reset player's xp score
xp set @s 0 levels
xp set @s 0 points
xp add @s -1 points
scoreboard players reset @s spl.xp

# add respawning tag
tag @s add spl.respawning
