# take all held chaos shards from the player
execute store result score #shards spl.tmp run clear @s <<items.chaos_shard.id>>{CustomModelData:<<items.chaos_shard.nbt.CustomModelData>>}

# add player points to team score
<% for team, teaminfo in teams %>
execute if entity @s[team=spl.<<team>>] run scoreboard players operation <<team>> spl.points += #shards spl.tmp

# for each player on the returning team, play bass noise
execute if entity @s[team=spl.<<team>>] as @a[team=spl.<<team>>] run playsound minecraft:block.note_block.bell player @s ~ ~ ~
# for each player *not* on the returning team, play bass noise
execute if entity @s[team=spl.<<team>>] as @a[team=!spl.<<team>>] run playsound minecraft:block.note_block.bass player @s ~ ~ ~
<% endfor %>

# if there were shards added, show effects
execute if score #shards spl.tmp matches 1.. run title @s subtitle {"text":"Your points have been added!"}
execute if score #shards spl.tmp matches 1.. run title @s title {"text":"Shards Returned"}
execute if score #shards spl.tmp matches 1.. run execute at @s run playsound minecraft:entity.player.levelup player @a ~ ~ ~
execute if score #shards spl.tmp matches 1.. run particle minecraft:end_rod ~ ~2 ~ 0 0 0 0.2 50
