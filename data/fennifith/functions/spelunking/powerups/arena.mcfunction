# Get player's current y position
execute as @a[tag=spl.started_powerup,limit=1] run execute store result score #posy spl.args run data get entity @s Pos[1] 1

# summon players around location, restricting below known y-pos
<% for height in range(-1, 11) %>
execute as @a[tag=spl.started_powerup,limit=1] at @s if score #posy spl.args matches ..<< (height * 32) - 8 >> run spreadplayers ~ ~ 3 20 under << height * 32 if height > 0 else 1 >> false @a
execute as @a[tag=spl.started_powerup,limit=1] at @s if score #posy spl.args matches ..<< (height * 32) - 8 >> run tag @s remove spl.started_powerup
<% endfor %>

# play teleport sound effect
execute as @a at @s run playsound minecraft:entity.enderman.teleport block @a ~ ~ ~
execute as @a at @s run particle minecraft:cloud ~ ~0.5 ~ 1 1 1 0 10

# apply glowing effect to leading players
effect give @a[tag=spl.leading] minecraft:glowing 30

# reset countdown scoreboard
scoreboard players set #arena spl.args 0
# release powerups lock
schedule function fennifith:spelunking/powerups/lock_release 30s replace
