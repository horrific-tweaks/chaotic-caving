execute store result score #effect spl.args run loot spawn ~ -65 ~ loot fennifith:spelunking/effect
execute positioned ~ -65 ~ run kill @e[type=item,distance=..1,nbt={Item:{id:"minecraft:dirt"}}]

execute as @a at @s run playsound minecraft:ui.button.click block @s ~ ~ ~

<% set effects = "slowness mining_fatigue jump_boost poison levitation teleport" %>
<% for effect in effects.split(" ") %>
execute if score #effect spl.args matches << loop.index0 >>.. run title @a title {"text":"<< effect.replace('_', ' ') | title >>","color":"yellow"}
<% endfor %>
