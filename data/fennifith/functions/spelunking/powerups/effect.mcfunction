# set powerups lock (prevent others from running during countdown)
scoreboard players set #lock_powerups spl.args 1
tag @a[tag=spl.started_powerup] remove spl.started_powerup
tag @s add spl.started_powerup

tellraw @a [{"selector":"@p","color":"aqua"},{"text":" has activated a random effect!","color":"white"}]

function fennifith:spelunking/powerups/effect_pick
schedule function fennifith:spelunking/powerups/effect_pick 5t append
schedule function fennifith:spelunking/powerups/effect_pick 10t append
schedule function fennifith:spelunking/powerups/effect_pick 15t append
schedule function fennifith:spelunking/powerups/effect_pick 21t append
schedule function fennifith:spelunking/powerups/effect_pick 28t append
schedule function fennifith:spelunking/powerups/effect_pick 38t append
schedule function fennifith:spelunking/powerups/effect_pick 50t append
schedule function fennifith:spelunking/powerups/effect_pick 3s append
schedule function fennifith:spelunking/powerups/effect_run 4s append
