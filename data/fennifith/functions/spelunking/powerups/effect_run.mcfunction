
execute store result score #effect spl.args run loot spawn ~ -65 ~ loot fennifith:spelunking/effect_actual
execute positioned ~ -65 ~ run kill @e[type=item,distance=..1,nbt={Item:{id:"minecraft:dirt"}}]

execute as @a at @s run playsound minecraft:ui.button.click block @s ~ ~ ~
execute as @a at @s run playsound fennifith:spelunking.horn_soft player @s ~ ~ ~
execute as @a at @s run playsound fennifith:spelunking.discomforting_harmony ambient @s ~ ~ ~ 99999 1 1

<% set effects = ["slowness", "mining_fatigue", "jump_boost", "poison", "levitation"] %>
<% for effect in effects %>
execute if score #effect spl.args matches << loop.index0 >> run title @a title {"text":"<< effect.replace('_', ' ') | title >>","color":"yellow"}
execute if score #effect spl.args matches << loop.index0 >> run effect give @a << effect >> 30 1
<% endfor %>

# if greater than len(effects), run arena teleport
execute if score #effect spl.args matches << effects|length >>.. run scoreboard players set #arena spl.args 1
execute if score #effect spl.args matches << effects|length >>.. run function fennifith:spelunking/powerups/arena_schedule

# release powerups lock
schedule function fennifith:spelunking/powerups/lock_release 30s replace
