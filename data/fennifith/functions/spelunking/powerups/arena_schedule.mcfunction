# set powerups lock (prevent others from running during countdown)
scoreboard players set #lock_powerups spl.args 1

<% set duration = 3 %>
<% for i in range(1, duration+1) %>
execute if score #arena spl.args matches << i >> run tellraw @a [{"text": "Teleporting to ","color":"yellow"},{"selector":"@a[tag=spl.started_powerup,limit=1]"},{"text":" in << duration + 1 - i >> seconds..."}]
execute if score #arena spl.args matches << i >> run title @a title {"text":"Teleport in << duration + 1 - i >>..."}
<% endfor %>

execute if score #arena spl.args matches ..<<duration>> as @a at @s run playsound minecraft:block.note_block.didgeridoo block @s ~ ~ ~
execute if score #arena spl.args matches ..<<duration>> run schedule function fennifith:spelunking/powerups/arena_schedule 1s

execute if score #arena spl.args matches << duration + 1 >> run title @a title {"text":""}
execute if score #arena spl.args matches << duration + 1 >> run function fennifith:spelunking/powerups/arena

scoreboard players add #arena spl.args 1
