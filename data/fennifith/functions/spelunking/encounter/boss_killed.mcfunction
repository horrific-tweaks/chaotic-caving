# Drop a mountain of shards
execute anchored eyes positioned ^ ^ ^2 run particle minecraft:wax_off ~ ~ ~ .5 1 .5 1 15

# Death effects
execute anchored eyes positioned ^ ^ ^2 run playsound minecraft:entity.wither.death hostile @a ~ ~ ~
execute anchored eyes positioned ^ ^ ^2 run playsound minecraft:entity.allay.death hostile @a ~ ~ ~

tellraw @a [{"selector":"@s","color":"aqua"},{"text":" has successfully defeated a boss battle!","color":"white"}]

# Reset the kill detection advancement
advancement revoke @s only fennifith:spelunking/boss_killed
