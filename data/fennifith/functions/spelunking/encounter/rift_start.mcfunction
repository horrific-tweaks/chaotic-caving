# pick how many enemies to spawn in encounter
#   - leading players get a higher enemy count of [5-9]
execute store result score #rift_level spl.tmp run loot spawn ~ -65 ~ loot fennifith:spelunking/random_20
execute positioned ~ -65 ~ run kill @e[type=item,distance=..1,nbt={Item:{id:"minecraft:dirt"}}]
#   - add min value to level
scoreboard players add #rift_level spl.tmp 5

# Summon the armor stand to act as a "rift"
summon armor_stand ~ ~ ~ {Tags:["spl.rift"],DisabledSlots:4144959,Invulnerable:1b,NoGravity:0b,Invisible:1b}

# copy #rift_level into armor_stand.Health
execute store result entity @e[type=armor_stand,tag=spl.rift,sort=nearest,limit=1] Health float 1 run scoreboard players get #rift_level spl.tmp

# create a bossbar for the rift
bossbar add spl.rift "Chaos Rift"
bossbar set spl.rift color purple
bossbar set spl.rift players @a[distance=..64]
# store max level of bossbar
execute store result bossbar spl.rift max run scoreboard players get #rift_level spl.tmp

# play sounds/effects
playsound minecraft:block.respawn_anchor.deplete player @a
playsound fennifith:spelunking.shatter player @a
effect give @a[distance=..16] minecraft:darkness 6

tellraw @a [{"selector":"@s","color":"aqua"},{"text":" activated a ","color":"white"},{"text":"lv.","color":"aqua"},{"score":{"name":"#rift_level","objective":"spl.tmp"}},{"text":" Chaos Rift!","color":"white"}]

# schedule rift loop function
schedule function fennifith:spelunking/encounter/rift_loop 10s replace

# lock to prevent other rift usage, schedule for 30s before next use
scoreboard players set #locked spl.rift 1
schedule function fennifith:spelunking/encounter/rift_unlock 30s replace
