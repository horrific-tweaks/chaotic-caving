# get the health of the current rift -> #rift_health spl.tmp
execute store result score #rift_health spl.tmp run data get entity @s Health

# pick which enemies to spawn
scoreboard players add #random_entity spl.rift 1
execute if score #random_entity spl.rift matches <<encounters.entities|length>>.. run scoreboard players set #random_entity spl.rift 0

# summon the chosen enemies
<% for entity in encounters.entities %>
<% set summon %>
execute if score #random_entity spl.rift matches <<loop.index0>> run <<entity>>
<% endset %>
execute if score #rift_health spl.tmp matches 1.. run <<summon|trim>>
execute if score #rift_health spl.tmp matches 1..3 run <<summon|trim>>
execute if score #rift_health spl.tmp matches 1..7 run <<summon|trim>>
<% endfor %>

# add chaos shard indicator to summoned entities
item replace entity @e[type=!player,distance=..1] armor.head with <<items.chaos_shard.id>><<items.chaos_shard.nbt|dump_nbt>>
# give resistance to mobs for 3s
effect give @e[type=!player,distance=..1] minecraft:resistance 3 10
# add tag to summoned entities
tag @e[type=!player,distance=..1] add spl.rift_spawned

# play sound/particles around spawned mobs
execute if score #rift_health spl.tmp matches 1.. run particle minecraft:dust 0 0 0 1 ~ ~1 ~ 1 1 1 1 50
execute if score #rift_health spl.tmp matches 1.. run playsound minecraft:item.chorus_fruit.teleport player @a
execute if score #rift_health spl.tmp matches 1.. as @a[distance=..64] at @s run playsound minecraft:entity.wither.spawn player @s ~ ~ ~ 0.2
execute if score #rift_health spl.tmp matches 1.. as @a[distance=..64] at @s run playsound minecraft:entity.warden.heartbeat player @s ~ ~ ~

# subtract 1 from the rift's health level
execute if score #rift_health spl.tmp matches 1.. run scoreboard players remove #rift_health spl.tmp 1
# store health level in rift entity
execute store result entity @s Health float 1 run scoreboard players get #rift_health spl.tmp
# store health level in rift bossbar
execute store result bossbar spl.rift value run scoreboard players get #rift_health spl.tmp
