# Summon a wither above the pedestal
summon wither ~ ~10 ~ {Invul:220}

# Wake up surrounding wither skeletons
execute as @e[type=minecraft:wither_skeleton,distance=..40] run data modify entity @s NoAI set value 0
execute as @e[type=minecraft:wither_skeleton,distance=..40] run data modify entity @s Invulnerable set value 0
execute as @e[type=minecraft:wither_skeleton,distance=..40] run effect clear @s

# Create loot in the chest
setblock ~ ~2 ~ air
loot spawn ~.5 ~2 ~.5 loot fennifith:spelunking/loot_chest_boss
particle minecraft:explosion_emitter ~ ~2 ~
playsound minecraft:entity.generic.explode block @a ~ ~2 ~

# Destroy this command block
setblock ~ ~ ~ air
setblock ~ ~1 ~ deepslate_tiles
setblock ~ ~-1 ~ deepslate_tiles
setblock ~1 ~ ~ deepslate_tiles
setblock ~-1 ~ ~ deepslate_tiles
setblock ~ ~ ~1 deepslate_tiles
setblock ~ ~ ~-1 deepslate_tiles
