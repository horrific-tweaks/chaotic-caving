# run spawn function for each fully-open (spl.rift=3) rift
execute as @e[type=armor_stand,tag=spl.rift,scores={spl.rift=3}] at @s run function fennifith:spelunking/encounter/rift_spawn

# play a sound effect / particle for growing rifts...
execute as @e[type=armor_stand,tag=spl.rift] unless score @s spl.rift matches 3.. at @s as @a[distance=..64] at @s run playsound minecraft:block.respawn_anchor.deplete player @s
execute as @e[type=armor_stand,tag=spl.rift] unless score @s spl.rift matches 3.. at @s as @a[distance=..64] at @s run playsound fennifith:spelunking.shatter player @s
execute as @e[type=armor_stand,tag=spl.rift] unless score @s spl.rift matches 3.. at @s run playsound minecraft:item.axe.scrape player @a
execute as @e[type=armor_stand,tag=spl.rift] unless score @s spl.rift matches 3.. at @s run particle minecraft:soul ~ ~1 ~ .5 1 .5 0.02 20
execute at @e[type=armor_stand,tag=spl.rift] run particle minecraft:cloud ~ ~ ~ 0 50 0 0 100

# play ambient sound
execute at @e[type=armor_stand,tag=spl.rift] run playsound minecraft:block.beacon.ambient player @a

# if rift is opening, perform open sequence
execute as @e[type=armor_stand,tag=spl.rift,scores={spl.rift=2}] run scoreboard players set @s spl.rift 3
execute as @e[type=armor_stand,tag=spl.rift,scores={spl.rift=1}] run scoreboard players set @s spl.rift 2
# for any rift...                            if it doesn't have a score yet...    set rift 1
execute as @e[type=armor_stand,tag=spl.rift] unless score @s spl.rift matches 1.. run scoreboard players set @s spl.rift 1

# for state 2, give players a warning...
execute as @e[type=armor_stand,tag=spl.rift,scores={spl.rift=2}] at @s run tellraw @a[distance=..48] [{"text":"The Chaos Rift is expanding... watch out for enemies!","color":"red"}]

# for states 1-3, replace head slot with custom rift item
item replace entity @e[type=armor_stand,tag=spl.rift,scores={spl.rift=1}] armor.head with minecraft:stick{CustomModelData:817331}
item replace entity @e[type=armor_stand,tag=spl.rift,scores={spl.rift=2}] armor.head with minecraft:stick{CustomModelData:817332}
item replace entity @e[type=armor_stand,tag=spl.rift,scores={spl.rift=3}] armor.head with minecraft:stick{CustomModelData:817333}

# if there are no more rift entities, remove the bossbar
execute unless entity @e[type=armor_stand,tag=spl.rift] run bossbar remove spl.rift

# nerf iron golems within radius
execute at @e[type=armor_stand,tag=spl.rift] as @e[type=iron_golem,distance=..32] run effect give @s weakness 10 2 true
execute at @e[type=armor_stand,tag=spl.rift] as @e[type=iron_golem,distance=..32] run data modify entity @s Health set value 1

# create an explosion above the entity
execute at @e[type=armor_stand,tag=spl.rift,scores={spl.rift=2..}] run summon minecraft:creeper ~ ~6 ~ {ExplosionRadius:3,Fuse:0,ActiveEffects:[{Id:14,Duration:999,ShowParticles:0b}]}

# schedule next iteration in 10s
execute if entity @e[type=armor_stand,tag=spl.rift] run schedule function fennifith:spelunking/encounter/rift_loop 10s replace
