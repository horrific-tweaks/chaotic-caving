# remove the trapped chest that activates this function
setblock ~ ~2 ~ air
# replace the command block with deepslate
setblock ~ ~ ~ deepslate

# play spawn effects
playsound minecraft:block.sculk_shrieker.shriek hostile @a ~ ~ ~
playsound minecraft:entity.lightning_bolt.thunder hostile @a ~ ~ ~
playsound minecraft:entity.lightning_bolt.impact hostile @a ~ ~ ~
particle minecraft:wax_off ~.5 ~3 ~.5 .5 1 .5 1 15

<% set mobs = ["zombie", "wither_skeleton", "husk"] %>

# get random value from 0-9
execute store result score #random spl.args run loot spawn ~ -65 ~ loot fennifith:spelunking/random_10
execute positioned ~ -65 ~ run kill @e[type=item,distance=..1,nbt={Item:{id:"minecraft:dirt"}}]

# modulus by number of mob options
scoreboard players set #max spl.args <<mobs|length>>
scoreboard players operation #random spl.args %= #max spl.args

<% for mob in mobs %>
# if <<mob>> is chosen...                                     summon two blocks above
execute if score #random spl.args matches <<loop.index0>> run summon <<mob>> ~ ~2 ~ {Tags:["spl.boss"]}
<% endfor %>

# set boss attributes
data modify entity @e[tag=spl.boss,limit=1] Attributes set value [{Name:"zombie.spawn_reinforcements",Base:1.0},{Name:"generic.attack_knockback",Base:2.0},{Name:"generic.knockback_resistance",Base:0.4}]
item replace entity @e[tag=spl.boss] weapon.mainhand with netherite_axe
item replace entity @e[tag=spl.boss] armor.chest with netherite_chestplate
item replace entity @e[tag=spl.boss] armor.legs with netherite_leggings
item replace entity @e[tag=spl.boss] armor.feet with netherite_boots
effect give @e[tag=spl.boss] resistance 999999 3 true
effect give @e[tag=spl.boss] slowness 999999 1 true
effect give @e[tag=spl.boss] jump_boost 999999 1 true

# copy exact rotation/pos from armor stand
data modify entity @e[tag=spl.boss,limit=1] Pos set from entity @e[type=armor_stand,sort=nearest,distance=..4,limit=1] Pos
data modify entity @e[tag=spl.boss,limit=1] Rotation set from entity @e[type=armor_stand,sort=nearest,distance=..4,limit=1] Rotation
kill @e[type=armor_stand,sort=nearest,distance=..4,limit=1]

# give player darkness effect for 15s
effect give @a[distance=..15] darkness 30 0
