# Summon a zombie as "backup"
#   get random chance for zombie to spawn
execute store result score #random spl.args run loot spawn ~ -65 ~ loot fennifith:spelunking/random_50
execute positioned ~ -65 ~ run kill @e[type=item,distance=..1,nbt={Item:{id:"minecraft:dirt"}}]
#   get the number of existing boss zombies
execute store result score #count spl.args if entity @e[tag=spl.boss_spider]
#   combine all spawn conditions into #can_spawn
scoreboard players set #can_spawn spl.tmp 0
#       if (50% chance)...                  unless 3 zombies already spawned...      as the closest boss...                   move two blocks behind    if block can be spawned inside
execute if score #random spl.args matches 1 unless score #count spl.args matches 3.. as @e[tag=spl.boss,sort=nearest,limit=1] at @s positioned ^ ^2 ^-2 if block ~ ~-1 ~ #fennifith:spelunking/raycast_ignore run scoreboard players set #can_spawn spl.tmp 1
execute if score #can_spawn spl.tmp matches 1 at @e[tag=spl.boss,sort=nearest,limit=1] positioned ^ ^2 ^-2 run summon spider ~ ~-1 ~ {Tags:["spl.boss_spider_init","spl.boss_spider"],Attributes:[{Name:"generic.attack_knockback",Base:2.0}],Health:8.0,Silent:1b,ActiveEffects:[{Id:14,Duration:999999}]}

#   play teleport effects
execute as @e[tag=spl.boss_spider_init] at @s run playsound minecraft:entity.enderman.teleport block @a ~ ~ ~
execute as @e[tag=spl.boss_spider_init] at @s run particle minecraft:cloud ~ ~0.5 ~ 1 1 1 0 10

#   remove identifier tag
tag @e[tag=spl.boss_spider_init] remove spl.boss_spider_init

# If boss is in/near a boat, remove the boat and drop item
<% set boats = ["acacia", "birch", "oak", "spruce", "dark_oak", "mangrove"] %>
<% for boat in boats %>
execute at @e[tag=spl.boss] if entity @e[type=boat,distance=..1,nbt={Type:"<<boat>>"}] run summon item ~ ~ ~ {Item:{id:"minecraft:<<boat>>_boat",Count:1}}
execute at @e[tag=spl.boss] if entity @e[type=chest_boat,distance=..1,nbt={Type:"<<boat>>"}] run summon item ~ ~ ~ {Item:{id:"minecraft:<<boat>>_chest_boat",Count:1}}
<% endfor %>
execute at @e[tag=spl.boss] if entity @e[type=minecart,distance=..1] run summon item ~ ~ ~ {Item:{id:"minecraft:minecart",Count:1}}

execute at @e[tag=spl.boss] run kill @e[type=boat,distance=..1]
execute at @e[tag=spl.boss] run kill @e[type=chest_boat,distance=..1]
execute at @e[tag=spl.boss] run kill @e[type=minecart,distance=..1]

# Increase boss speed
#   store health in spl.tmp
execute as @e[tag=spl.boss] store result score @s spl.tmp run data get entity @s Health
#   where health < 15, speed 0
execute as @e[tag=spl.boss,scores={spl.tmp=..10}] run effect give @s speed 30 0
#   where health < 10, speed 1
execute as @e[tag=spl.boss,scores={spl.tmp=..6}] run effect give @s speed 30 1
#   where health < 5, speed 2
execute as @e[tag=spl.boss,scores={spl.tmp=..3}] run effect give @s speed 30 2

# effect noise
execute at @e[tag=spl.boss,sort=nearest,limit=1] run playsound minecraft:entity.wither.hurt hostile @s ~ ~ ~

# Reset the hit detection advancement
advancement revoke @s only fennifith:spelunking/boss_hit
