scoreboard players set #hit spl.cast 1

# create a 'marker' entity in the center of the mined block
execute positioned ^ ^ ^-0.1 align xyz run summon marker ~ ~ ~ {Tags:["spl.mined"]}
