# increase time value (once per minute)
scoreboard players add #timer_mins spl.args 1

# set bossbar time value
scoreboard players set #_break spl.args 0
<% for minute in range(0, config.minutes) %>
<% set remaining = config.minutes - minute %>
execute unless score #_break spl.args matches 1 if score #timer_mins spl.args matches ..<< minute >> run bossbar set spl.bar value << remaining >>
execute unless score #_break spl.args matches 1 if score #timer_mins spl.args matches ..<< minute >> run bossbar set spl.bar name "Time Remaining: << (remaining / 60) | round(0, 'floor') >>h << remaining % 60 >>m"
execute unless score #_break spl.args matches 1 if score #timer_mins spl.args matches ..<< minute >> run scoreboard players set #_break spl.args 1
<% endfor %>

# when timer is over -> on_end
execute if score #timer_mins spl.args matches << config.minutes >> run function fennifith:spelunking/timer/on_end