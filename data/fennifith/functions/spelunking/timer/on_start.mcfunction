# inits the bossbar to show remaining time for players
bossbar add spl.bar "Time Remaining"
bossbar set spl.bar color pink
bossbar set spl.bar style notched_20
bossbar set spl.bar visible true
bossbar set spl.bar max << config.minutes >>
bossbar set spl.bar players @a

# set timer ticks active
scoreboard players set #timer spl.args 1
scoreboard players set #timer_mins spl.args 0
bossbar set spl.bar max << config.minutes >>
# - 60m option
execute if score #opt_time spl.global matches 1 run scoreboard players set #timer_mins spl.args 30
execute if score #opt_time spl.global matches 1 run bossbar set spl.bar max << config.minutes - 30 >>
# - 30m option
execute if score #opt_time spl.global matches 2 run scoreboard players set #timer_mins spl.args 60
execute if score #opt_time spl.global matches 2 run bossbar set spl.bar max << config.minutes - 60 >>
# ensure bossbar is updated on next tick
scoreboard players set #timer_tick spl.args 1199

# start title/horn
title @a title {"text":"Start!"}
execute as @a at @s run playsound fennifith:spelunking.timpani player @s ~ ~ ~ 1 .5
execute as @a at @s run playsound fennifith:spelunking.gong player @s ~ ~ ~ 99999 1 1
execute as @a at @s run playsound fennifith:spelunking.discomforting_harmony ambient @s ~ ~ ~ 99999 1 1
schedule function fennifith:spelunking/events/loop_ambience 60s append
schedule function fennifith:spelunking/events/loop_effects 1s replace
schedule function fennifith:spelunking/events/loop_find_structures 1s replace
schedule function fennifith:spelunking/events/loop_music 60s replace

# reset scores & start game
gamemode survival @a[team=!]
gamemode spectator @a[team=]

tag @a[team=!] add spl.player
