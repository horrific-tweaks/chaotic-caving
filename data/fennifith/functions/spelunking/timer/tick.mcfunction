# if timer is running, increment tick variable
execute if score #timer spl.args matches 1 run scoreboard players add #timer_tick spl.args 1

# invoke function every 1200t (1 minute)
execute if score #timer spl.args matches 1 if score #timer_tick spl.args matches 1200 run scoreboard players set #timer_tick spl.args 0
execute if score #timer spl.args matches 1 if score #timer_tick spl.args matches 0 run function fennifith:spelunking/timer/increment
