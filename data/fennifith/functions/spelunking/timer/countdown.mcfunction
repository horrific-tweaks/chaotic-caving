execute unless score #countdown spl.args matches 1.. run scoreboard players set #countdown spl.args 11
scoreboard players remove #countdown spl.args 1

<% for i in range(1, 11) %>
execute if score #countdown spl.args matches <<i>> run tellraw @a [{"text":"<<i>> second<< "s" if i > 1 else "" >>...","color":"yellow"}]
execute if score #countdown spl.args matches <<i>> run title @a title {"text":"<<i>>"}
<% endfor %>

execute if score #countdown spl.args matches 1.. as @a at @s run playsound fennifith:spelunking.timpani block @s ~ ~ ~

execute if score #countdown spl.args matches 1.. run schedule function fennifith:spelunking/timer/countdown 1s replace
execute unless score #countdown spl.args matches 1.. run function fennifith:spelunking/timer/on_start