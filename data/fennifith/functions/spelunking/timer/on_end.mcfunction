# hide the bossbar
bossbar set spl.bar visible false

# set timer ticks inactive
scoreboard players set #timer spl.args 0

# clear encounters if running
kill @e[tag=spl.rift]
kill @e[tag=spl.rift_spawned]
bossbar remove spl.rift
schedule clear fennifith:spelunking/encounter/rift_loop

# reset locks
scoreboard players set #locked spl.rift 0

# only announce winning team if game is running (i.e. not a hook_reset)
execute if score #global_running spl.args matches 1 run function fennifith:spelunking/points/on_end

# put all players into spectator mode
gamemode spectator @a[tag=spl.player]
