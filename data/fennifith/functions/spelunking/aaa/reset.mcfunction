# clear xp points for all players
xp set @a 0 points
xp set @a 0 levels
xp add @a -1

scoreboard players reset * spl.points

# reset encounters
bossbar remove spl.encounter_bar
tag @a remove spl.encounter_tagger
scoreboard players reset * spl.encounter_tag
